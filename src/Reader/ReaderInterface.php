<?php

namespace Benchmark\Reader;

interface ReaderInterface
{
    /**
     * Try to read from given url
     * @param string $url url which will be read
     */
    public function readUrl(string $url);
}