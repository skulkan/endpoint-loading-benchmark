<?php

namespace Benchmark\Reader;

class FileGetContentsReader implements ReaderInterface
{
    /**
     * @inheritdoc
     */
    public function readUrl(string $url)
    {
        file_get_contents($url);
    }
}