<?php

namespace Benchmark\Timer;

class MicroTimeTimer implements TimerInterface
{
    /**
     * @var float
     */
    private $startTime;

    /**
     * @inheritdoc
     */
    public function start(): bool
    {
        $restarted = $this->startTime !== null ? true : false;

        $this->startTime = $this->getMicroTimeInSeconds();

        return $restarted;
    }

    /**
     * @inheritdoc
     */
    public function getTimeInSeconds(): float
    {
        return $this->getMicroTimeInSeconds() - $this->startTime;
    }

    /**
     * @return float current Unix timestamp with microseconds
     */
    private function getMicroTimeInSeconds(): float
    {
        return microtime(true);
    }
}