<?php

namespace Benchmark\Timer;

/**
 * Interface TimerInterface Counting time from run method time and returning current diff while run getTimeInSeconds
 */
interface TimerInterface
{
    /**
     * Start counting time, restart if already started
     *
     * @return bool true if timer was restarted
     */
    public function start(): bool;

    /**
     * @return float time from object initialization to now in seconds with microseconds
     */
    public function getTimeInSeconds(): float;
}