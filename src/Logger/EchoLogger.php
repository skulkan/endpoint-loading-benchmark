<?php

namespace Benchmark\Logger;

class EchoLogger implements LoggerInterface
{
    /**
     * @inheritdoc
     */
    public function log(string $message)
    {
        echo $message . "\n";
    }
}