<?php

namespace Benchmark\Logger;

interface LoggerInterface
{
    /**
     * Log message
     *
     * @param string $message message to be logged
     */
    public function log(string $message);
}