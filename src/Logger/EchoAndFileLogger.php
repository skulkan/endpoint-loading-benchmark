<?php

namespace Benchmark\Logger;

class EchoAndFileLogger implements LoggerInterface
{
    const FILE_PATH = __DIR__ . '/../../var/log.txt';

    /**
     * @inheritdoc
     */
    public function log(string $message)
    {
        $message .= "\n";

        file_put_contents(self::FILE_PATH, $message, FILE_APPEND);
        echo $message;
    }
}