<?php

namespace Benchmark;

use Benchmark\Reader\ReaderInterface;
use Benchmark\Rule\RuleInterface;
use Benchmark\Timer\TimerInterface;
use Benchmark\Logger\LoggerInterface;

class Benchmark
{
    /**
     * @var TimerInterface
     */
    private $timer;

    /**
     * @var ReaderInterface
     */
    private $reader;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var RuleInterface[]
     */
    private $rules = [];

    /**
     * Benchmark constructor.
     * @param TimerInterface $timer
     * @param ReaderInterface $reader
     * @param LoggerInterface $logger
     */
    public function __construct(TimerInterface $timer, ReaderInterface $reader, LoggerInterface $logger)
    {
        $this->timer = $timer;
        $this->reader = $reader;
        $this->logger = $logger;
    }
    
    public function registerRule(RuleInterface $rule)
    {
        $this->rules[] = $rule;
    }

    /**
     * @param string $endpoint
     * @param string[] $competitors
     */
    public function compareEndpointWithOthers(string $endpoint, array $competitors)
    {
        $this->logger->log(sprintf(
            "Benchmark starts at %s",
            (new \DateTime())->format('c'))
        );

        $this->logger->log(sprintf(
            "Benchmarking first endpoint (%s)",
            $endpoint
        ));
        $time = $this->benchmarkEndpoint($endpoint);
        $this->logger->log(sprintf(
            "Result is %.6f",
            $time
        ));

        $bestCompetitorTime = 0.0;
        foreach ($competitors as $competitor) {
            $this->logger->log(sprintf(
                "Benchmarking second endpoint (%s)",
                $competitor
            ));
            $timeB = $this->benchmarkEndpoint($competitor);
            $this->logger->log(sprintf(
                "Result is %.6f",
                $timeB
            ));
            if ($timeB < $bestCompetitorTime || $bestCompetitorTime == 0.0) {
                $bestCompetitorTime = $timeB;
            }
        }

        $this->checkRules($time, $bestCompetitorTime);
    }

    /**
     * @param string $endpoint
     * @return float
     */
    private function benchmarkEndpoint(string $endpoint): float
    {
        $this->timer->start();
        $this->reader->readUrl($endpoint);

        return $this->timer->getTimeInSeconds();
    }

    /**
     * @param float $time
     * @param float $bestCompetitorTime
     */
    private function checkRules(float $time, float $bestCompetitorTime)
    {
        foreach ($this->rules as $rule) {
            $rule->compare($time, $bestCompetitorTime);
        }
    }
}