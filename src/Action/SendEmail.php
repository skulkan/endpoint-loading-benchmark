<?php

namespace Benchmark\Action;

use Benchmark\Logger\LoggerInterface;

class SendEmail implements ActionInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SendEmail constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function execute()
    {
        $this->logger->log("These are not the emails you're looking for :)");
    }
}