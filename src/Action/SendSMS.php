<?php

namespace Benchmark\Action;

use Benchmark\Logger\LoggerInterface;

class SendSMS implements ActionInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SendEmail constructor.
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function execute()
    {
        $this->logger->log("This is not the SMS you're looking for :)");
    }
}