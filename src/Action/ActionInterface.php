<?php

namespace Benchmark\Action;

interface ActionInterface
{
    public function execute();
}