<?php

namespace Benchmark\Rule;

/**
 * Class ActionNotRegistered throws when rules
 */
class ActionNotRegistered extends \Exception
{
}