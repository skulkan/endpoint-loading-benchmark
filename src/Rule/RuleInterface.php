<?php

namespace Benchmark\Rule;

use Benchmark\Action\ActionInterface;

interface RuleInterface
{
    /**
     * Compares two floats regards internal rules and run or not action
     *
     * @param float $timeA
     * @param float $timeB
     * @throws ActionNotRegistered
     */
    public function compare(float $timeA, float $timeB);

    /**
     * @param ActionInterface $action
     */
    public function setAction(ActionInterface $action);
}