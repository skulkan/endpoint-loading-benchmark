<?php

namespace Benchmark\Rule;

use Benchmark\Action\ActionInterface;

class FirstTwiceBiggerThanSecond implements RuleInterface
{
    /**
     * @var ActionInterface
     */
    private $action;

    /**
     * @inheritdoc
     */
    public function compare(float $timeA, float $timeB)
    {
        if ($this->action == null) {
            throw new ActionNotRegistered;
        }

        if ($timeA > ($timeB * 2)) {
            $this->action->execute();
        }
    }

    /**
     * @inheritdoc
     */
    public function setAction(ActionInterface $action)
    {
        $this->action = $action;
    }
}