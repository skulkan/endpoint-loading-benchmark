# Endpoint loading benchmark #

### How to set up ###
* Checkout the code
* Run composer install

### How to use it ###
From the project root run

php benchmark.php http://myDomain.example.com http://competitorDomain.example.com http://otherCompetitorDomain.com

It is posible to test two or more domains

### How it works ###
* It's made single call to any of given domain (only for given url, it doesn't try to download any more related files from html: css, js, images, etc).
* It's use one process so second link is measured after measure of first one and go on to last one. Execution time is sum of all request + some computation time.
* After this call's it's compare request time for first domain with best time of others domains requests.
