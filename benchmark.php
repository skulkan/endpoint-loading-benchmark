#!/usr/bin/php
<?php

if (count($argv) < 3) {
    exit(
        "Provide two or more endpoints for comparision"
    );
}

require_once('vendor/autoload.php');

use Benchmark\Action\SendEmail;
use Benchmark\Action\SendSMS;
use Benchmark\Benchmark;
use Benchmark\Logger\EchoAndFileLogger;
use Benchmark\Reader\FileGetContentsReader;
use Benchmark\Rule\FirstBiggerThanSecond;
use Benchmark\Rule\FirstTwiceBiggerThanSecond;
use Benchmark\Timer\MicroTimeTimer;

$benchmark = new Benchmark(new MicroTimeTimer(), new FileGetContentsReader(), new EchoAndFileLogger());

$action1 = new SendEmail(new EchoAndFileLogger());
$rule1 = new FirstBiggerThanSecond();
$rule1->setAction($action1);
$benchmark->registerRule($rule1);

$action2 = new SendSMS(new EchoAndFileLogger());
$rule2 = new FirstTwiceBiggerThanSecond();
$rule2->setAction($action2);
$benchmark->registerRule($rule2);

$benchmark->compareEndpointWithOthers($argv[1], array_slice($argv, 2));