<?php

namespace Test\Benchmark\Rule;

use Benchmark\Action\ActionInterface;
use Benchmark\Rule\FirstBiggerThanSecond;
use PHPUnit\Framework\TestCase;

final class FirstBiggerThanSecondTest extends TestCase
{
    /**
     * @expectedException \Benchmark\Rule\ActionNotRegistered
     */
    public function testWithoutAction()
    {
        $rule = new FirstBiggerThanSecond();
        $rule->compare(0.0, 0.0);
    }

    /**
     * @param float $numberA
     * @param float $numberB
     * @param bool $actionExecuted
     * @dataProvider providerForTestWithAction
     */
    public function testWithAction(float $numberA, float $numberB, bool $actionExecuted)
    {
        $action = $this->createMock(ActionInterface::class);
        $action->expects($spy = $this->any())
            ->method('execute');

        $rule = new FirstBiggerThanSecond();
        $rule->setAction($action);
        $rule->compare($numberA, $numberB);

        $invocations = $spy->getInvocations();

        if ($actionExecuted) {
            $this->assertEquals(1, count($invocations));
        } else {
            $this->assertEquals(0, count($invocations));
        }
    }

    public function providerForTestWithAction()
    {
        return [
            [0.0, 0.0, false],
            [2.0, 1.0, true],
            [1.0, 2.0, false],
            [1.0, 0.999999, true],
            [0.999999, 1.0, false]
        ];
    }
}